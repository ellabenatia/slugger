//  returns a single string with hyphens between each word like so:
//  i-am-a-slugged-string-i-contain-no-spaces
export default function slugger(...args){
    for(let i =0; i<args.length; i++){
        args[i] = args[i].trim().replaceAll(" ", '-');
    }
    return [...args].join("-");  
}
