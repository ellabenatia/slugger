import slugger from "./index.js";

/**
 * @describe [optional] - group of tests with a header to describe them
 */
 describe("slugger", () => {
    it("should be defined", () => {
        expect(slugger).toBeDefined();
      });
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(slugger("ab cd","gg")).toEqual("ab-cd-gg");
    })
    it('slugger can slug string with spaces', () => {
         expect(slugger("ab bc cd")).toEqual("ab-bc-cd");
     })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
       // ...your code here
        expect(slugger("a","b")).toEqual("a-b");
    })
})


  