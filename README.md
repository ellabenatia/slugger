# SLUGGER 
slugger accepts any number of strings as arguments and returns a single string with hyphens between each word.
## installation
```
npm i @ellabenatia/slugger
```

## CommonJS


## ES6 modules
```javascript
import marker from '@ellabenatia/slugger'; 
```
